// Security details
package main

import (
	"strings"
	"time"

	"github.com/shopspring/decimal"
)

func calculateSecurityDetails(symbol string, currency string) map[string]string {
	result := make(map[string]string)

	result["yield"] = getYield(symbol, currency)
	result["g/l"] = getGainLoss(symbol, currency)

	return result
}

// Calculate the yield in the last 12 months.
// This, of course is affected by the recent purchases, which affect the current value!
func getYield(symbol string, currency string) string {
	// Calculate the yield in the last 12 months.
	// This, of course is affected by the recent purchases, which affect the current value!
	incomeStr := get_income_balance(symbol, currency)
	income, err := decimal.NewFromString(incomeStr)
	if err != nil {
		panic(err)
	}
	// turn into a positive number
	income = income.Abs()

	// get the current value
	valueStr := get_value_balance(symbol, currency)
	value, err := decimal.NewFromString(valueStr)
	if err != nil {
		panic(err)
	}

	var yield decimal.Decimal
	if value.Equal(decimal.Zero) {
		yield = decimal.Zero
	} else {
		// yield = income * 100 / value  => percentage
		yield = income.Mul(decimal.NewFromInt(100)).Div(value)
	}

	result := yield.StringFixedBank(2) + "%"
	return result
}

func get_value_balance(symbol string, currency string) string {
	cmd := []string{"b", "^Assets", "and", ":" + symbol + "$", "-X", currency}
	output, err := runLedger(cmd...)
	if err != nil {
		panic(err)
	}

	lines := SplitLines(output)
	value := get_total_from_ledger_output(lines)

	if value == "" {
		value = "0"
	}

	return value
}

// Gets the balance of income for the security
func get_income_balance(symbol string, currency string) string {
	// Get the income in the last year.
	yieldFrom := getDateYearAgo()

	ledgerCmd := []string{"b", "^Income", "and", ":" + symbol + "$", "-b", yieldFrom, "--flat",
		"-X", currency}
	output, err := runLedger(ledgerCmd...)
	if err != nil {
		panic(err)
	}
	lines := SplitLines(output)

	total := get_total_from_ledger_output(lines)
	if total == "" {
		total = "0"
	}
	return total
}

// Extract the total value from ledger output
func get_total_from_ledger_output(ledgerOutput []string) string {
	if len(ledgerOutput) == 0 {
		return ""
	}

	totalLine := get_total_lines(ledgerOutput)[0]
	totalNumeric := extract_total(totalLine)
	return totalNumeric
}

// Gets the numeric value of the total from the ledger total line
func extract_total(totalLine string) string {
	// Extract the numeric value of the income total.

	totalLine = strings.TrimSpace(totalLine)

	parts := strings.Split(totalLine, " ")
	totalNumeric := parts[0]
	totalNumeric = strings.ReplaceAll(totalNumeric, ",", "")

	return totalNumeric
}

// Returns a short ISO date exactly a year ago.
func getDateYearAgo() string {
	//yield_start_date = date.today() - timedelta(weeks=52)
	today := time.Now()
	startDate := today.AddDate(-1, 0, 0)
	// ISO time format
	result := startDate.Format("2006-01-02")
	return result
}

// Get gain/loss from ledger
func getGainLoss(symbol string, currency string) string {
	ledger_cmd := []string{"b", "^Assets", "and", ":" + symbol + "$", "-G", "-n", "-X", currency}

	ledgerOutput, err := runLedger(ledger_cmd...)
	if err != nil {
		panic(err)
	}
	if ledgerOutput == "" {
		return "0 " + currency
	}

	// get number
	number := get_number_from_collapse_result(ledgerOutput)

	result := number + " " + currency

	return result
}

// Parses a 1-line ledger result, when --collapse is used
func get_number_from_collapse_result(source string) string {
	source = strings.TrimSpace(source)

	// -1,139 EUR  Assets
	parts := strings.Split(source, " ")
	if len(parts) != 4 {
		panic("wrong number of parts!")
	}

	number := parts[0]
	number = strings.ReplaceAll(number, ",", "")

	return number
}
