// Tests for Lots
package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLotsRun(t *testing.T) {
	lots, err := getLots("VEUR")

	assert.Nil(t, err)
	assert.NotNil(t, lots)
	assert.NotEmpty(t, lots[0])
}
