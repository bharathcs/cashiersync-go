# cashiersync-go

CashierSync project, implemented in Go(lang).

The [original project](https://gitlab.com/alensiljak/cashiersync) is written in Python. This project effectively supersedes the old. 
It is an experiment in Go, but should remain the active project due to characteristics of the platform, which are better suited to the tasks that CashierSync is performing.

# Purpose

todo

# Installation

To install, run
```
go install gitlab.com/alensiljak/cashiersync-go@latest
```

# Run

Run `cashiersync-go.exe` in a console. This runs the Web API server which listens for the calls from the Cashier client.

Ledger-cli must be in the path, executable from the current location.
It is advised to have a `.ledgerrc` file in the current directory, to set up Ledger data sources.

# Development

Requirements:

- go (v1.17)

## Requests

Requests are located in the `cashiersync.rest` file and can be executed using VSCode's REST Client extension.

## Hot Reload

To use hot reload during development, simply install Fresh package

```
go install github.com/pilu/fresh
```

and then run `fresh` from the project root directory. It will monitor source code files for changes and automatically rebuild the application.

# Deployment

Binaries are published on GitLab.

https://gitlab.com/alensiljak/cashiersync-go/-/releases

The module is published in Go package repository:

https://pkg.go.dev/gitlab.com/alensiljak/cashiersync-go

# License

GNU General Public License v3.0

See [license](LICENSE).
