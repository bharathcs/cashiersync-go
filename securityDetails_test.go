// Security details tests
package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSecurityCalculation(t *testing.T) {
	result := calculateSecurityDetails("VHYL", "EUR")

	assert.NotNil(t, result)
	assert.NotEmpty(t, result)
}

func TestYield(t *testing.T) {
	result := getYield("VEUR", "EUR")

	assert.NotNil(t, result)
}

func TestYieldMultiline(t *testing.T) {
	result := getYield("VHY_AX", "EUR")

	assert.NotNil(t, result)
}
