// Lots operations
package main

import (
	"strings"
)

// Returns lots for a given symbol.
func getLots(symbol string) ([]string, error) {
	params := []string{"b", "^Assets", "and", "invest", "and", ":" + symbol + "$",
		"--lots", "--no-total", "--collapse"}
	// run ledger
	ledgerOutput, err := runLedger(params...)
	if err != nil {
		return nil, err
	}

	if ledgerOutput == "" {
		return nil, err
	}

	// split lines
	//result := strings.Split(ledger_output, "\r\n")
	result := SplitLines(ledgerOutput)

	// remove "Assets" account title
	lastIndex := len(result) - 1
	lastLine := result[lastIndex]
	if strings.Contains(lastLine, "Assets") {
		parts := strings.Split(lastLine, "Assets")
		value := strings.TrimSpace(parts[0])
		result[lastIndex] = value
	}

	return result, nil
}
